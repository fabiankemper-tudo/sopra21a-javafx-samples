package application;
	
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import view.MainWindowViewController;


public class Main extends Application {

	@Override
	public void start(Stage primaryStage) {
		try {
			MainWindowViewController mainViewController = new MainWindowViewController();
			Scene scene = new Scene(mainViewController, 600, 480);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (Exception error) {
			error.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}
}
